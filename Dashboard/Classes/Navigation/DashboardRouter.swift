//
//  AppBaseRouter.swift
//  Core
//
//  Created by Aditi Jain 3 on 29/05/22.
//

import Foundation

public class DashboardRouter:NSObject{
    
    public static func routeTo(identifier:String)->UIViewController{
            let bundle = Bundle.init(identifier: "org.cocoapods.Dashboard")
            let storyboard = UIStoryboard(name: "Dashboard", bundle: bundle)
            let nxtVC = storyboard.instantiateViewController(withIdentifier: identifier)
            return nxtVC
    }
}
