//
//  ViewController.swift
//  Dashboard
//
//  Created by Aditi Jain 3 on 28/05/22.
//

import UIKit
import Core

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
      //  "".printMyNameV2()
        "Aditi".printMyName()
        // Do any additional setup after loading the view.
    }

    @IBAction func onTapOfActionButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as? UITabBarController else {
            return
        }
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: true)
    }
    
}

